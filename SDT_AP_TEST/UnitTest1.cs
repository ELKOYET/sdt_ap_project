using System;
using Xunit;
using AirPollutionLib;
using Microsoft.Office.Interop;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;

namespace SDT_AP_TEST
{
    public class UnitTest1
    {

        [Fact]

        public void Test1()
        {
            const double i = 10.35;
            ClassAirPollutionInput AirPollutionInput = new ClassAirPollutionInput();
            AirPollutionInput.D = 6.4;
            AirPollutionInput.Tr = 100;
            AirPollutionInput.Ta = 30;
            AirPollutionInput.Zo = 0.1;
            AirPollutionInput.V = 1198800;
            AirPollutionInput.A = 160;
            AirPollutionInput.nu = 75;
            AirPollutionInput.E = 0;
            ClassAirPollutionCount AirPollutionCount = new ClassAirPollutionCount();
            AirPollutionCount.AirPollutionInput = AirPollutionInput;

            //Assert.Equal(i, AirPollutionCount.Wo_AverageVelocityOutfall());
            //Assert.Equal(333, AirPollutionCount.V1_MixtureVolume());
            //Assert.Equal(33.3, AirPollutionCount.M_DustAmount());
            Assert.Equal(1.53, AirPollutionCount.Xm_TorchDistance);
            //Assert.Equal(0.82, AirPollutionCount.m_Coefficient());
            //Assert.Equal(4.3, AirPollutionCount.Vm_Coefficient());

        }
      
    }




    
}
