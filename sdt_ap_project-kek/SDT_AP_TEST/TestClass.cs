﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;


namespace SDT_AP_TEST
{
    class TestClass

    {
        public TestClass(string jopa)
        {
            ExcelRead(jopa);
        }
        public  double[] Testdata;
        public void ExcelRead(string jopa)
        {
            Application ex = new Application();
            Workbook exBook = ex.Workbooks.Open(jopa);
            Worksheet exSheets;
            exSheets = (Worksheet)exBook.Sheets[1];
            Microsoft.Office.Interop.Excel.Range result = exSheets.UsedRange.Range[exSheets.Cells[13, 8], exSheets.Cells[16,8 ]] as Microsoft.Office.Interop.Excel.Range;


            Array resultarray = (Array)result.Cells.Value2;
            Testdata = resultarray.OfType<object>().Select(o => Convert.ToDouble(o)).ToArray();
            
        }
    }
}
