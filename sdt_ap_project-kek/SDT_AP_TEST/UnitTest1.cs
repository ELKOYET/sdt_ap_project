using System;
using Xunit;
using AirPollutionLib;
using Microsoft.Office.Interop;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using NUnit.Framework;

namespace SDT_AP_TEST
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
             string jopa =@"C:\Users\Andrew\Desktop\sdt_ap_project-kek\KURSACH.xlsx";
        ClassAirPollutionInput AirPollutionInput = new ClassAirPollutionInput();
            ClassAirPollutionCount AirPollutionCount = new ClassAirPollutionCount();
            AirPollutionCount.AirPollutionInput = AirPollutionInput;
         

            TestClass test = new TestClass(jopa);





        AirPollutionInput.H = 80;
            AirPollutionInput.D = 6.4;
            AirPollutionInput.Tr = 100;
            AirPollutionInput.Ta = 30;
            AirPollutionInput.Zo = 0.1;
            AirPollutionInput.V = 1198800;
            AirPollutionInput.A = 160;
            AirPollutionInput.nu = 75;
            AirPollutionInput.E = 0;
            double[] Array1 = new double[4] {0.192, 0.455, 0.925, 0.976};

            NUnit.Framework.Assert.AreEqual(10.3565, AirPollutionCount.Wo_AverageVelocityOutfall());
            NUnit.Framework.Assert.AreEqual(333, AirPollutionCount.V1_MixtureVolume());
            NUnit.Framework.Assert.AreEqual(33.3, AirPollutionCount.M_DustAmount());
            NUnit.Framework.Assert.AreEqual(test.Testdata[0], AirPollutionCount.f_Coefficient());
            NUnit.Framework.Assert.AreEqual(test.Testdata[1], AirPollutionCount.m_Coefficient());
            NUnit.Framework.Assert.AreEqual(test.Testdata[2], AirPollutionCount.Vm_Coefficient());
            NUnit.Framework.Assert.AreEqual(1, AirPollutionCount.n_Coefficient());
            NUnit.Framework.Assert.AreEqual(2.5, AirPollutionCount.F_Coefficient());
            NUnit.Framework.Assert.AreEqual(0.059743499999999998, AirPollutionCount.Cm_MaxPollutionConcentration());
            NUnit.Framework.Assert.AreEqual(19.221, AirPollutionCount.d_Coefficient());
            NUnit.Framework.Assert.AreEqual(76.884, AirPollutionCount.Xm_TorchDistance());
            NUnit.Framework.Assert.AreEqual(4.9500000000000002, AirPollutionCount.um_DangerousWindVelocity());
            NUnit.Framework.Assert.AreEqual(Array1, AirPollutionCount.r_CoefficientCount());
        }
    }   






}
